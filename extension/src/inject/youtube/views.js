checkUpdate();

function bot() {
    var config = {
        childList: true, attributes: true, characterData: true, subtree: true,
        attributeOldValue: false, characterDataOldValue: true, attributeFilter: []
    };

    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            var addedNodes = mutation.addedNodes;

            loopNodes(addedNodes, function (node) {
                if (node.nodeType === 1 && node.tagName === 'CENTER') {
                    captchaSolve(node);
                    startBot(node);
                }
            });
        });
    });

    observer.observe(document.getElementById('site-links-list'), config);
}

function loopNodes(nodeList, callback) {
    for (var index in nodeList) {
        if (nodeList.hasOwnProperty(index)) {
            callback(nodeList[index]);
        }
    }
}

function startBot(node) {
    if (node.firstElementChild.className === 'yt-video-content') {
        var startButton = document.getElementsByClassName('single_like_button btn3-wrap');

        if (startButton) {
            startButton[0].click();
        }
    }
}

function captchaSolve(node) {
    if (node.firstElementChild.className === 'img_capt_container') {
        var captchaOptions = {
            notification: 'captcha',
            closeAfter: 3000,
            captchaImages: {
                'image1': document.getElementById('images0').src,
                'image2': document.getElementById('images1').src
            },
            settings: {
                'type': 'basic',
                'title': 'AddMeFast Notification',
                'message': 'Captcha Found.',
                'iconUrl': '/icons/icon-alert.png',
                'contextMessage': 'Youtube Views',
                'isClickable': true
            }
        };

        sendChromeMessage(captchaOptions, function (response) {
            console.log(response);
        });
    }
}

function checkUpdate() {
    var updateOptions = {
        notification: 'update',
    };

    sendChromeMessage(updateOptions, function (updateResponse) {
        if (updateResponse.requiresUpdate) {
            alert(updateResponse.message);
        } else {
            bot();
        }
    });
}

function sendChromeMessage(messageData, callback) {
    chrome.extension.sendMessage(messageData, callback);
}